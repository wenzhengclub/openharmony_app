import router from '@system.router';
export default {
    data: {
        title: 'World'
    },
    onInit(){
        console.log('---------------------------------- 1s');
        setTimeout(function(){
            console.log('---------------------------------- 1s');
            router.replace({
                uri:"pages/main/main",
                params: {
                    data1: 'message',
                },
            });
        },1000);
    },
}