// @ts-nocheck
//灯状态 0是关闭 1是开启
var led = {open:1,close:0,change:2}
var imu = {get_angel:1}
var e53 = {motor_on:0, motor_off:1,get_sensor:2}
import router from '@system.router';
import device from '@system.device';
import app from '@system.app';
export default {
    data: {
        buttonInfo: 'close',
        sensor_val:'',
        temperature:28.36,
        humidity:56.8,
        light:80,
        statu:'0',
        angle:70,
        animation:'',
        interval_1:'',
        interval_2:'',
        width:'',
        height:'',
        scaleX:'',
        rotate:'',
        rotateX:'',
        rotateY:'',
    },
    exit(e){
        app.terminate()
    },


    onScalex: function() {
        this.scaleX = "";
        this.scaleX = "scaleX";
    },

    onRotate: function() {
        this.rotate = "";
        this.rotate = "rotate";
    },

    onRotateX: function() {
        this.rotateX = "";
        this.rotateX = "rotateX";
    },

    onRotateY: function() {
        this.rotateY = "";
        this.rotateY = "rotateY";
    },
    open(e){
        let that = this
        app.ledcontrol({
            code:led.open,
            success(res){
                that.statu = res.led_status
            },
            fail(res,code){

            },
            complete(){

            }
        })
    },
    close(e){
        let that = this
        app.ledcontrol({
            code:led.close,
            success(res){
                that.statu = res.led_status
            },
            fail(res,code){

            },
            complete(){

            }
        })
    },
    change(e){
        let that = this
        app.ledcontrol({
            code:led.change,
            success(res){
                that.statu = res.led_status
            },
            fail(res,code){

            },
            complete(){

            }
        })
    },
    touchMove(e){  // swipe处理事件
        if(e.direction == "up"){
            this.open()
        }else if(e.direction == "down"){
            this.close()
        }else if(e.direction == "right"){
            this.exit()
        }else if(e.direction == "left"){

        }
    },
    touchE53(e){  // swipe处理事件
        let that = this
        if(e.direction == "up"){
            this.open()
        }else if(e.direction == "down"){
            this.close()
        }else if(e.direction == "right"){

        }else if(e.direction == "left"){

        }
    },

    interval_start(e){
        let that = this
        //实时监测屏幕旋转
        that.interval_1 = setInterval(function(){
            app.getimuangle({
                code:imu.get_angel,
                success(res){
                    that.angle = res.imu_status
                    if(that.angle <= 50){
                        //this.open()
                    }else{
                        //this.close()
                    }
                },
                fail(res,code){
                },
                complete(){
                }
            })
        },200);
    },

    interval_getsensor_start(e){
        let that = this
        //实时监测屏幕旋转
        that.interval_2 = setInterval(function(){
            app.e53control({
                code:e53.get_sensor,
                success(res){
                    that.sensor_val = res.e53_replay
                    console.log('---------------------------------- ');
                    console.log(res.e53_replay);
                    var jsonObj = JSON.parse(res.e53_replay);
                    that.temperature = jsonObj.t;
                    that.humidity = jsonObj.h;
                    that.light = jsonObj.l;
                },
                fail(res,code){

                },
                complete(){

                }
            })
        },500);
    },





    interval_stop(e){
        let that = this
        clearInterval(that.interval)
    },

    button_click(e) {
        let that = this
        let codmsg = e53.motor_on

        if(that.buttonInfo == "open"){
            that.buttonInfo = "close"
            codmsg = e53.motor_off
        }else{
            that.buttonInfo = "open"
            codmsg = e53.motor_on
        }
        this.interval_stop()
        app.e53control({
            code:codmsg,
            success(res){
                console.log('---------------------------------- ');
                console.log(res.e53_replay);
            },
            fail(res,code){

            },
            complete(){

            }
        })
        this.interval_start()
    },

    onInit(){
        console.log('--------------------onInit-------------- ');
        this.interval_start();
        this.interval_getsensor_start();

    },





}
