import router from '@system.router';

var led = {open:1,close:0,change:2}
var imu = {get_angel:1}
var e53 = {motor_on:0, motor_off:1,get_sensor:2}
import app from '@system.app';

export default {
    data: {
        on_flag:0,
        motor_animation: '',
        led_animation: '',
        motor_src: 0,
        led_src: false,

    },
    onInit(){

    },
    ledchange(e){
        app.ledcontrol({
            code:led.change,
            success(res){
            },
            fail(res,code){

            },
            complete(){

            }
        })
    },

    motor_click(e) {
        this.motor_animation = '';
        this.motor_animation = "motor_animationChange";
        this.motor_src = true;

        if(this.on_flag != 0){
            this.led_animation = "led_animationrestore";
            this.led_src = false;
            this.ledchange();
        }
        this.on_flag = 1;

        app.e53control({
            code:e53.motor_on,
            success(res){
                console.log('---------------------------------- ');
                console.log(res.e53_replay);
            },
            fail(res,code){

            },
            complete(){

            }
        })

    },

    led_click(e) {
        this.led_animation = '';
        this.led_animation = "led_animationChange";
        this.led_src = true;

        if(this.on_flag != 0){
            this.motor_animation = "motor_animationrestore";
            this.motor_src = false;
        }
        this.on_flag = 1;
        this.ledchange();
    },

    center_click(e) {
        router.replace({
            uri:"pages/main/main",
            params: {
                data1: 'message',
            },
        });
    },

}

